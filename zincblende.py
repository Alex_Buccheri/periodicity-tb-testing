#!/usr/bin/env python3

#Modules
import numpy as np
import sys
import constants


# Would be cool to have a function that determines if they're stored
# row-wise or column-wise
class Primitive_zinc_blende:
    # Basis vectors in reduced units
    basis = np.array([[0.,   0.,   0.],
                      [0.25, 0.25, 0.25]])

    # Lattice vectors in reduced units
    # [ [a1,a2,a3]
    #   [b1,b2,b3]
    #   [c1,c2,c3] ]
    lattice = np.array([[0.5, 0.5, 0.],
                        [0., 0.5, 0.5],
                        [0.5, 0., 0.5]])

class Unit_zinc_blende:
    # Basis vectors in reduced units
    # (taken from my original TB code)
    basis = np.array([[0.,   0.,   0.  ],
                      [0.,   0.5,  0.5 ],
                      [0.5,  0.,   0.5 ],
                      [0.5,  0.5,  0.  ],
                      [0.25, 0.25, 0.25],
                      [0.25, 0.75, 0.75],
                      [0.75, 0.25, 0.75],
                      [0.75, 0.75, 0.25]])

    # Lattice vectors in reduced units
    # [ [a1,a2,a3]
    #   [b1,b2,b3]
    #   [c1,c2,c3] ]
    lattice = np.array([[1.,  0.,  0.],
                        [0.,  1.,  0.],
                        [0.,  0.,  1.]])



class BZ:
    #Private
    # High symmetry points in units of 2 pi/al (reduced crystalographic)
    _X=np.array([0.,1.,0.])
    _T=np.array([0.,0.,0.])
    _L=np.array([0.5,0.5,0.5])
    _U=np.array([0.25,1.,0.25])
    _K=np.array([0.75,0.75,0.])
    _W=np.array([0.5,1.,0.])
    #Public
    k_units='reduced'
    symmetry_point={'X':_X,'T':_T,'L':_L,'U':_U,'K':_K,'W':_W}

# This assignment works when basis is the inner loop in
# supercell construction - consistent with definitions of basis, above
def assign_zb_basis_species(cation,anion,nbasis):
    basis_species = []
    i2 = int(nbasis * 0.5)
    for i in range(0,i2):
        basis_species.append(anion)
    for i in range(i2,nbasis):
        basis_species.append(cation)
    return basis_species

#Set basis and lattice vectors
def select_basis(atom_type,al,lattice_choice):
    cation = atom_type['cation']
    anion = atom_type['anion']

    if lattice_choice == 'prim':
        basis_positions = al * Primitive_zinc_blende.basis
        nbasis = constants.basis_size(basis_positions)
        basis_species = assign_zb_basis_species(cation, anion, nbasis)
        lattice = al * Primitive_zinc_blende.lattice.transpose()

    elif lattice_choice == 'orth':
        basis_positions = al * Unit_zinc_blende.basis
        nbasis = constants.basis_size(basis_positions)
        basis_species = assign_zb_basis_species(cation, anion, nbasis)
        lattice = al * Unit_zinc_blende.lattice.transpose()

    else:
        sys.exit('Erroneous choice of lattice vectors')
    return basis_positions,basis_species,lattice
