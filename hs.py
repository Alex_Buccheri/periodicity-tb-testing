#!/usr/bin/env python3

#Modules
import numpy as np
import sys
#My modules
import constants
import structure


# -------------------------
# Construct H and S
# -------------------------

# Returns a periodic Hamiltonian or overlap matrix for the specified 'system', built from
# a Hamiltonian/overlap matrix for a larger supercell
def construct_periodic_HS_kpoints(system, species, equivalent_atoms, full_H, kvec, r):
    N = structure.total_basis_size(system, species)
    H = np.zeros(shape=(N,N), dtype=np.dtype(np.complex64))

    ia=0
    for iatom in system:
        ibasis = constants.basis_per_atom[species[iatom]]
        i1, i2 = start_stop_indices_orbital_block(ia,len(ibasis))

        ja = 0
        for jatom in system:
            jbasis = constants.basis_per_atom[species[jatom]]
            j1, j2 = start_stop_indices_orbital_block(ja, len(jbasis))

            jatom_in_other_cells = equivalent_atoms[jatom]
            for equiv_jatom in jatom_in_other_cells:
                tvec = r[equiv_jatom,:] - r[iatom,:]

                # Can use jbasis here as all atoms have same basis in test material
                block = get_block(full_H, iatom, equiv_jatom, ibasis, jbasis)
                H[i1:i2,j1:j2] += np.exp(1j*np.dot(kvec,tvec))* block.astype(complex)
            ja+=1
        ia+=1

    return H


def get_block(A,iatom,jatom,ibasis,jbasis):
    i1, i2 = start_stop_indices_orbital_block(iatom, len(ibasis))
    j1, j2 = start_stop_indices_orbital_block(jatom, len(jbasis))
    row = range(i1,i2)
    col = range(j1,j2)
    return A[np.ix_(row,col)]


#nbasis = basis size for atom ia
def start_stop_indices_orbital_block(ia,nbasis):
    i1 = 0 + ia * nbasis
    i2 = (nbasis) + ia * nbasis
    return i1,i2


#Hamiltonian and overlap file names
def read_in_entos_hs(hfile,sfile):
    h0 = np.loadtxt(hfile)
    s = np.loadtxt(sfile)

    #print('Not checking that input H,S are Hermitian to save time')
    # Are the matrices symmetric?
    if not np.allclose(h0, h0.T, atol=1.e-8):
        print('h0 not symmetric')
        sys.exit('Script stops')
    if not np.allclose(s, s.T, atol=1.e-8):
        print('s not symmetric')
        sys.exit('Script stops')

    return h0,s


# Nearest neighbour Hamiltonian
def construct_NN_periodic_HS_kpoints(system, species, n_list, full_H, kvec, r):
    N = structure.total_basis_size(system, species)
    H = np.zeros(shape=(N,N), dtype=np.dtype(np.complex64))
    np.set_printoptions(linewidth=640, formatter={'float': '{: 0.6f}'.format})

    ia=0
    for iatom in system:
        ibasis = constants.basis_per_atom[species[iatom]]
        i1, i2 = start_stop_indices_orbital_block(ia,len(ibasis))

        ja = 0
        for jatom in system:
            jbasis = constants.basis_per_atom[species[jatom]]
            j1, j2 = start_stop_indices_orbital_block(ja, len(jbasis))

            # On-site (else capture 2nd NN)
            if iatom == jatom:
                jatom_in_other_cells = [jatom]
            # Inter-atomic
            else:
                jatom_in_other_cells = n_list[iatom]

            for equiv_jatom in jatom_in_other_cells:
                tvec = r[equiv_jatom,:] - r[iatom,:]
                block = get_block(full_H, iatom, equiv_jatom, ibasis, jbasis)
                print(ia, ja, iatom, jatom, equiv_jatom)
                print(block)
                H[i1:i2,j1:j2] += np.exp(1j*np.dot(kvec,tvec))* block.astype(complex)
            ja+=1
        ia+=1

    return H


def print_real_HS(h0,s0):
    np.set_printoptions(linewidth=640,formatter={'float': '{: 0.6f}'.format})
    print('H')
    print(h0.real*constants.ha_2_ev)
    #np.set_printoptions()
    print('')
    print('H-H^T')
    print( (h0-h0.T).real*constants.ha_2_ev )
    print('Overlap')
    print(s0.real)
    print('')
    print('S-S^T')
    print(s0.real - s0.T.real)
    print('')
    return


