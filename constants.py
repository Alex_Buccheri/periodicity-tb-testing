#!/usr/bin/env python3

import sys

#Conversions
ang_2_bohr = 1./0.52917721067
bohr_2_ang = 0.52917721067
ha_2_ev = 27.2114

#Imaginary i
i_img = complex(0,1)  #Equally valid i_img = 0.+1.j

#Number of nearest neighbours in bulk
nearest_n = 4

#Lattice constants (Angstrom, 300k)
#si ref: http://www.ioffe.ru/SVA/NSM/Semicond/Si/basic.html
al_ang={'unity':1,'si':5.431}

basis_per_atom={'si':['s','p','p','p','d','d','d','d','d']}

def al(material,unit='bohr'):
    if material not in al_ang:
        print(material,' lattice constant not tabulated')
        sys.exit('Code has stopped')
    if unit=='bohr':
        return al_ang[material]*ang_2_bohr
    elif unit=='angstrom':
        return al_ang[material]
    else:
        print('Choice of unit is erronoeus:',unit)
        sys.exit('Code has stopped')

def bond_length(al):
    return 0.25*(3**.5)*al

def basis_size(basis):
    return len(basis[:,0])

def number_of_atoms(basis_size,nx,ny,nz):
    return int(basis_size*len(nx)*len(ny)*len(nz))

#Ref: https://en.wikipedia.org/wiki/Diamond_cubic
def neighbour_distance(al):
    NN   = bond_length(al)
    NN2 = 0.5*(2.**0.5)*al
    NN3 = 0.25*(11.**0.5)*al
    NN4 = 1.*al
    NN5 = 0.25*(19.**0.5)*al
    NN6 = 1.224744871391589*al  #Inferred from the code
    NN8 = 1.732050807568877*al  #Inferred from the code
    ndist = {1:NN, 2:NN2, 3:NN3, 4:NN4, 5:NN5, 6:NN6, 8:NN8}

    return ndist