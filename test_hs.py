#!/usr/bin/env python3

#Modules
import numpy as np

#My modules
import hs

# ----------------------------------------------------------
# Test H/S construction using the functions:
#  'construct_finite_HS' and 'get_overlap_block'
# Note: Total basis size hard-coded in construct_finite_HS and construct_periodic_HS
# Not tested for systems larger than 2x2 atoms
# Not tested get periodic_block but not much that can go wrong
#
# I can do something like this in the main script to make sure everything's ok
# ----------------------------------------------------------
natoms = 5 # [0,1,2,3,4]
species= []
for i in range(0,natoms):
    species.append('si')

nbasis = 4 #Consistent with si
N = natoms*nbasis

v = np.arange(0,N,1)
H = np.outer(v,v)
np.fill_diagonal(H, 1)
S = np.zeros(shape=(N,N))

if not np.allclose(H, H.T, atol=1.e-8):
    print('H not symmetric')


#No need to do IO every time
# np.savetxt('htest.dat',H)
# H_in = np.loadtxt('htest.dat')
# if not np.allclose(H, H_in, atol=1.e-8):
#     print('different arrays')

#List atoms in final system - only need to work for a 2x2 system
system = [1,3]

atomi = system[0]
atomj = system[1]

#Sub H between atoms ia and ja
i1, i2 = hs.start_stop_indices_orbital_block(atomi,nbasis)
j1, j2 = hs.start_stop_indices_orbital_block(atomj,nbasis)
H_ii = H[i1:i2,i1:i2]
H_jj = H[j1:j2,j1:j2]
H_ij = H[i1:i2,j1:j2]

#These look good
#print(H_ii)
#print(H_jj)
#print(H_ij)

#Put these sub-Hamiltonians into a new 2x2 H
H_1 = np.zeros(shape=(2*nbasis,2*nbasis))
i1, i2 = hs.start_stop_indices_orbital_block(0,nbasis)
j1, j2 = hs.start_stop_indices_orbital_block(1,nbasis)
H_1[i1:i2,i1:i2] = H_ii[:,:]
H_1[j1:j2,j1:j2] = H_jj[:,:]
H_1[i1:i2,j1:j2] = H_ij[:,:]
H_1[j1:j2,i1:i2] = H_ij[:,:].T

#Construct sub H with routine
H_2, S_2 = hs.construct_finite_HS(system,species,H,S)

# CHeck that the matrices are the same
# print(H_1)
# print(H_2)
if not np.allclose(H_1, H_2, atol=1.e-8):
    print('H1 and H2 disagree')
else:
    print('H_1 and H_2 agree')

