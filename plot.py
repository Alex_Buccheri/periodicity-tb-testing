#!/usr/bin/env python3

#My modules
import constants

# Output in .xyz:
#Inputs:
# fname = file name
# natoms = list of atom indices
# origin = origin (just hard-coded)
# species = species list
# r  = atomic coordinates of supercell
def output_xyz(fname,natoms,r,species):
    if fname[-4:] != '.xyz':
        fname += '.xyz'
    fid = open(file=fname,mode='w')
    print(len(natoms), file=fid)
    print('0. 0. 0.', file=fid)
    for ia in natoms:
        print(species[ia], r[ia,0], r[ia,1], r[ia,2], file=fid)
    fid.close()
    return

def output_list_in_xyz(fname,system):
    print('Currently have species hard-coded for this output')
    if fname[-4:] != '.xyz':
        fname += '.xyz'
    fid = open(file=fname,mode='w')
    print(len(system), file=fid)
    print('0. 0. 0.', file=fid)
    for atom in system:
        print('si', atom[0], atom[1], atom[2], file=fid)
    fid.close()
    return