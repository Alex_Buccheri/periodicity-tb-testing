#!/usr/bin/env python3

#Modules
import numpy as np
import sys
#My modules
import constants

# Create supercell
# Document the linear algebra here
def supercell(nx,ny,nz,basis,lattice):
    nbasis = constants.basis_size(basis['pos'])
    natoms = constants.number_of_atoms(nbasis,nx,ny,nz)
    r = np.zeros(shape=(natoms, 3))
    atom_to_cell = np.zeros(shape=(natoms))
    species = []
    cell_to_atom = []
    ia=0
    icell = 0

    for ix in nx:
        for iy in ny:
            for iz in nz:
                # Keep track of cells (for atom index to be contiguous
                # in cell, basis must be the inner loop)
                cell_to_atom.append(np.arange(ia,ia+nbasis))
                for ib in range(0,nbasis):
                    r[ia,:] = basis['pos'][ib,:] + np.matmul(lattice,[ix,iy,iz])
                    species.append(basis['species'][ib])
                    atom_to_cell[ia] = int(icell)
                    ia+=1

                icell+=1

    return r,cell_to_atom,atom_to_cell,species



#Works for cubic cell, not sure if it would for other cells
def outside_central_cell(pos, basis):
    is_outside = False
    max_atom_in_cell = np.amax(basis['pos'])+1.e-4
    if (pos[0] < 0. or pos[0] > max_atom_in_cell) or \
       (pos[1] < 0. or pos[1] > max_atom_in_cell) or \
       (pos[2] < 0. or pos[2] > max_atom_in_cell):
        is_outside = True
    return is_outside

#Return atoms in central cell and surrounding cells
def atoms_in_central_cell(r,r0,basis):
    central_cell = []
    other_cells = []
    natoms = len(r)
    for ia in range(0, natoms):
        if not outside_central_cell(r[ia, :] - r0, basis):
            central_cell.append(r[ia,:].tolist())
        elif outside_central_cell(r[ia, :] - r0, basis):
            other_cells.append(r[ia,:].tolist())
    return central_cell, other_cells


# Expect lattice ordered:
# [ [a1,b1,c1]
#   [a2,b2,c2]
#   [a3,b3,c3] ]
# def translation_vector(pos,lattice):
#     a = lattice[:,0]
#     b = lattice[:, 0]
#     c = lattice[:, 0]
#     ix = int(pos[0]/np.linalg.norm(a))
#     iy = int(pos[1]/np.linalg.norm(b))
#     iz = int(pos[2]/np.linalg.norm(c))
#     tvec = np.matmul(lattice, [ix, iy, iz])
#     return tvec
#
#Doesn't work properly - was meant to rely on the rounding of
#the (ix,iy,iz) integers to the appropriate cell with translation_vector, above
# def standardise_positions(r,r0,lattice):
#     rnew = []
#     natoms = len(r)
#     for ia in range(0,natoms):
#         if outside_central_cell(r[ia,:]-r0, lattice):
#             tvec = translation_vector(r[ia,:]-r0,lattice)
#             new_position =   r[ia,:] - tvec[:]
#             rnew.append(new_position.tolist())
#     return rnew


# r = (r'-r0) + basis_position + T
# hence r' = r - basis_position - T (and we don't care about redefining the origin)
# Below should be invariant to r0, hence superflous
def alt_standardise_positions(r,r0,basis,lattice):
    nbasis = constants.basis_size(basis['pos'])
    natoms = len(r)
    tol = 1.e-6
    rnew = []
    nx=5
    ny=5
    nz=5
    for ix in range(-nx,nx+1):
        for iy in range(-ny, ny+1):
            for iz in range(-nz, nz+1):
                tvec = np.matmul(lattice,[ix,iy,iz])
                for ib in range(0,nbasis):
                    b_pos = basis['pos'][ib, :]

                    for ia in range(0,natoms):
                        if np.linalg.norm(r[ia,:]-r0-b_pos-tvec) < tol:
                            #print(ix, iy, iz, ia, np.linalg.norm(r[ia, :] - r0 - tvec))
                            new_position = r[ia, :] - tvec[:]
                            rnew.append(new_position.tolist())
    return rnew


def total_basis_size(system, species):
    size = 0
    for iatom in system:
        size += len(constants.basis_per_atom[species[iatom]])
    return size


#Map atomic index to (ix,iy,iz,ibasis) or vice versa
#Return   indices[iatom][0:3] = [ix,iy,iz,ibasis]
#         atom_map[ix,iy,iz,ibasis] = iatom
def supercell_indices(nx,ny,nz,nbasis):
    atom_map = np.zeros(shape=(len(nx),len(ny),len(nz),nbasis))
    indices = []
    ia=0
    icell = 0
    for ix in nx:
        for iy in ny:
            for iz in nz:
                for ib in range(0,nbasis):
                    indices.append([ix,iy,iz,ib])
                    atom_map[ix,iy,iz,ib] = ia
                    ia+=1
                icell+=1

    return atom_map,indices



def cell_indexing(cell_to_atom,nlist):

    # All cells
    all_cells = np.arange(0,len(cell_to_atom))

    # Make a list of all cells that contain at least one surface atom
    # If a cell contains a surface atom, the cell can't be fully coordinated
    icell=0
    surface_cells=[]
    for cell in cell_to_atom:
        for atom in cell:
            neighbours_of_atom = nlist[atom]
            if len(neighbours_of_atom) < 5:
                surface_cells.append(icell)
        icell += 1

    # Bulk (full-coordinated) cells
    central_cells = list(set(all_cells)-set(surface_cells))
    return surface_cells,central_cells


# Create neighbour list, include the atom's own index
# Won't work if any atom doesn't have neighbours
def find_neighbours(r,cutoff):
    natoms = r.shape[0]
    nlist = []
    tol=0.0001

    for ia in range(0,natoms):
        local_list = []
        for ja in range(0,natoms):
            if np.linalg.norm(r[ja,:]-r[ia,:]) <= cutoff+tol:
                local_list.append(ja)
        nlist.append(local_list)
    return nlist



# Find specific neighbours
# neighbours = [1,2,4] corresponding to 1st, 2nd and 4th nearest neighbours, respectively
def find_specific_neighbours(neighbours,r,al):
    natoms = r.shape[0]
    tol = 0.01
    neighbour_distances = constants.neighbour_distance(al)
    nlist = []

    for ia in range(0,natoms):
        local_list = []
        for ja in range(0,natoms):
            separation = np.linalg.norm(r[ja, :] - r[ia, :])

            #Find neighbours at specific distances
            for n in neighbours:
                neigh_dist = neighbour_distances[n]
                if (separation < neigh_dist+tol) and (separation > neigh_dist-tol):
                    local_list.append(ja)
        nlist.append(local_list)

    return nlist



# Find neighbouring cells of a given central cell
# Note, if an adjacent cell does not contain a nearest neighbour of an atom in the
# central cell, this routine will not find it
# Note 2: Feel like I should be able to do this with an integer grid, and just look around it

def find_neighbouring_cells(icentral,nlist,cell_to_atom,atom_to_cell):
    cell = []
    centre_cell_atoms = cell_to_atom[icentral]

    # Make a list of nearest neighbours of all atoms in central cell
    for atom in centre_cell_atoms:
        neighbours = nlist[atom]
        # For each neighbouring atom, get its cell
        for neighbour in neighbours:
            cell.append(atom_to_cell[neighbour])

    # Remove duplicates and central cell index
    return list(set(cell)-{icentral})


#For a fixed basis position, find corresponding atom in each neighbouring cell
#Fully-coordinated central cell is surrounded by 26 other cells
def find_neighbouring_cell_atoms(iatom,atom_map,indices):

    nx_ic = indices[iatom][0]
    ny_ic = indices[iatom][1]
    nz_ic = indices[iatom][2]
    basis_ic = indices[iatom][3]
    #same_atom_in_adj_cells = np.zeros(shape=(26))
    same_atom_in_adj_cells = []

    # Iterate over surrounding cells, excluding central cell
    cnt=0
    for ix in range(nx_ic-1,nx_ic+2):
        for iy in range(ny_ic - 1, ny_ic + 2):
            for iz in range(nz_ic - 1, nz_ic + 2):
                if cnt != 13:
                    same_atom_in_adj_cells.append(int(atom_map[ix,iy,iz,basis_ic]))
                cnt+=1
    return same_atom_in_adj_cells


#For 6 surrounding cells
def find_closest6_cell_atoms(iatom,atom_map,indices):

    ix = indices[iatom][0]
    iy = indices[iatom][1]
    iz = indices[iatom][2]
    ibasis = indices[iatom][3]
    same_atom_in_adj_cells = []

    same_atom_in_adj_cells.append(int(atom_map[ix+1, iy, iz, ibasis]) )
    same_atom_in_adj_cells.append(int(atom_map[ix-1, iy, iz, ibasis]) )
    same_atom_in_adj_cells.append(int(atom_map[ix, iy+1, iz, ibasis]) )
    same_atom_in_adj_cells.append(int(atom_map[ix, iy-1, iz, ibasis]) )
    same_atom_in_adj_cells.append(int(atom_map[ix, iy, iz+1, ibasis]) )
    same_atom_in_adj_cells.append(int(atom_map[ix, iy, iz-1, ibasis]) )

    return same_atom_in_adj_cells

#I based the above routine on this
#If a cell contains a surface atom, the cell can't be fully coordinated
# def check_central_cell_indexing(central_cells,cell_to_atom,nlist):
#     #Make a list of all cells that contain at least one surface atom
#     icell=0
#     surface_cells=[]
#     for cell in cell_to_atom:
#         for atom in cell:
#             neighbours_of_atom = nlist[atom]
#             if len(neighbours_of_atom) < 5:
#                 surface_cells.append(icell)
#         icell += 1
#     print('Surface cells:')
#     print(surface_cells)
#     print('Bulk cells:')
#     print(central_cells)
#
#     # If any of the cells in surfaces cells is in central_cells, there's an error
#     # in my definition of central_cells
#     print('List intersection. Should not contain any elements')
#     print( set(surface_cells).intersection(set(central_cells)) )
#     return surface_cells





