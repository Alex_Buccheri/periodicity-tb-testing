#!/usr/bin/env python3

# Modules
import numpy as np
import sys
from scipy import linalg

#Local modules
import zincblende as zb
import constants
import structure
import plot
import hs

# --------------------------------------------
# Test exponential function - runs no problem
# --------------------------------------------

# Set up material type and supercell dimensions
material = 'si'
atom_types = {'cation':'si','anion':'si'}
lattice_choice = 'prim'
nx = np.arange(0,3)
ny = np.arange(0,3)
nz = np.arange(0,3)

# Basis
al = constants.al(material,unit='bohr')
basis_positions,basis_species,lattice = zb.select_basis(atom_types,al,lattice_choice)
nbasis = len(basis_species)
basis={'pos':basis_positions,'species':basis_species}
natoms = constants.number_of_atoms(nbasis,nx,ny,nz)

# Supercell and cell indexing
r, cell_to_atom, atom_to_cell, species = structure.supercell(nx,ny,nz,basis,lattice)


kvec = [1.,1.,1.]

for iatom in range(0,natoms):
    for jatom in range(0,natoms):
        tvec = r[iatom,:] - r[jatom,:]
        print(np.exp(complex(0,1)*np.dot(kvec,tvec)))
