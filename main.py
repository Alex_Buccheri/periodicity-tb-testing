#!/usr/bin/env python3

# Modules
import numpy as np
import sys
from scipy import linalg

#Local modules
import zincblende as zb
import constants
import structure
import plot
import hs
import bandstructure as bs

# ---------------------------------------------------
# Routine to test construction of periodic H and S
# Alex Buccheri March 2019
# ---------------------------------------------------

# -------------
# User choices
# -------------
#This should all stay fixed for a given input of (h_data,s_data)
material = 'si'
atom_types = {'cation':'si','anion':'si'}

# Generate 3D supercell, with one fully-coordinated, central cell
# Once chosen, must keep consistent with H and S read from file
lattice_choice = 'prim'
nx = np.arange(0,5)
ny = np.arange(0,5)
nz = np.arange(0,5)


# Basis
al = constants.al(material,unit='bohr')
basis_positions,basis_species,lattice = zb.select_basis(atom_types,al,lattice_choice)
nbasis = len(basis_species)
basis={'pos':basis_positions,'species':basis_species}
natoms = constants.number_of_atoms(nbasis,nx,ny,nz)

# Supercell and cell indexing
r, cell_to_atom, atom_to_cell, species = structure.supercell(nx,ny,nz,basis,lattice)
atom_map,indices = structure.supercell_indices(nx,ny,nz,nbasis)

#Output supercell
label=lattice_choice+str(len(nx))+str(len(ny))+str(len(nz))
plot.output_xyz('xyz_files/supercell_'+label+'.xyz',np.arange(0,natoms),r*constants.bohr_2_ang,species)

# Nearest neighbour, surface cells and central cells lists
bond_length = constants.bond_length(al)
nlist = structure.find_neighbours(r,bond_length)
surface_cells, central_cells = structure.cell_indexing(cell_to_atom,nlist)

if not central_cells:
    print('No cell is fully coordinated')
    sys.exit('Script has stopped')


#Find bulk atoms and output
bulk_atoms = []
for ia in range(0,natoms):
    if len(nlist[ia]) == constants.nearest_n+1:
        bulk_atoms.append(ia)
plot.output_xyz('xyz_files/bulk',bulk_atoms,r*constants.bohr_2_ang, species)

#Find surface atoms and output
surface_atoms = []
for ia in range(0,natoms):
    if len(nlist[ia]) < constants.nearest_n + 1:
        surface_atoms.append(ia)
plot.output_xyz('xyz_files/surface',surface_atoms,r*constants.bohr_2_ang,species)


#List atoms in central cells and surface cells, and output
centralcell_atom_indices=[]
surfacecell_atom_indices=[]
for ia in range(0,natoms):
    if atom_to_cell[ia] in central_cells:
        centralcell_atom_indices.append(ia)
    else:
        surfacecell_atom_indices.append(ia)
plot.output_xyz('xyz_files/bulk_cell',centralcell_atom_indices,r*constants.bohr_2_ang,species)
plot.output_xyz('xyz_files/surface_cell',surfacecell_atom_indices,r*constants.bohr_2_ang,species)

print('List of central cells, each comprised of ',nbasis,'atoms:')
print(central_cells)


# Central cell
# Any element from central_cells, as all fully-coordinated
icentral = 58  #central_cells[0]

# List of atoms in central cell define the system
system = cell_to_atom[icentral]
print('Choosing system to comprise of atoms:',system,' in cell:',icentral)
print('')

#Output atoms defining 2-atom system and their NN
plot.output_xyz('xyz_files/new_system',system,r*constants.bohr_2_ang,species)
system_neighbours = []
for ia in system:
    for neighbours in nlist[ia]:
        system_neighbours.append(neighbours)
plot.output_xyz('xyz_files/NN',system_neighbours,r*constants.bohr_2_ang,species)


# For each atom in the system, get a list of equivalent atoms in adjacent unit cells
# Include itself in atom_in_other_cells
equivalent_atoms = {}
for atom in system:
    atom_in_other_cells = structure.find_neighbouring_cell_atoms(atom, atom_map, indices)
    atom_in_other_cells.append(atom)
    equivalent_atoms[atom] = atom_in_other_cells


print('Central cell atom, NEquivAtoms, equivalent atom in adjacent cells:')
for atom in system:
    print(atom, len(equivalent_atoms[atom]), equivalent_atoms[atom])
print('')

# Export atoms in surrounding cells (put all surrounding atoms into one list)
surrounding_cells = []
for atom in system:
    surrounding_cells += equivalent_atoms[atom][:-1]
plot.output_xyz('xyz_files/surrounding_cells',surrounding_cells,r*constants.bohr_2_ang,species)
plot.output_xyz('xyz_files/2_atom_system',system,r*constants.bohr_2_ang,species)


# --------------------------------------------------------
# Quantifying Nth Neighbours and Corresponding Distances
# --------------------------------------------------------
def round_list_precision(mylist,precision):
    return list(np.around(np.array(mylist), precision))

def get_key_with_value(mydict,value):
    return list(mydict.keys())[list(mydict.values()).index(value)]


print('Tabulated Neighbour distances: ', constants.neighbour_distance(al))
# Puts dictionary values into a set
tabulated_neigbours = set(constants.neighbour_distance(al)[key] for key in constants.neighbour_distance(al))

#Create reverse dictionary for neighbour distances
reverse_neighbours = {}
for key in constants.neighbour_distance(al):
    value = constants.neighbour_distance(al)[key]
    reverse_neighbours[np.around(value,8)] = key

# Distance vector
d_mat = []
distances = []
for atom in system:
    for equi_atom in equivalent_atoms[atom]:
        dvec = r[equi_atom,:] - r[atom,:]
        seperation =  np.around(np.linalg.norm(dvec),8)  #Rounding to make comparison below
        distances.append(seperation)
    d_mat.append(distances)


# print('Separation between central and equivalent atoms in adjacent cells')
# for ia in range(0,len(system)):
#     print (ia, d_mat[ia])

computed_neighbours = []
for ia in range(0,len(system)):
    atom = system[ia]
    for ja in range(0,len(equivalent_atoms[atom])):
        if d_mat[ia][ja] in reverse_neighbours:
            neighbour_index = reverse_neighbours[d_mat[ia][ja]]
            computed_neighbours.append(neighbour_index)
        elif d_mat[ia][ja]==0.:
            pass
        else:
            print('Erroneous neighbour distance found for atom pair: ', d_mat[ia][ja])
            #sys.exit('Code has stopped')

print('Atoms in adajcent cells correspond to ith nearest neighbours: ',set(computed_neighbours))

# Missing NN, 3rd NN, 5th NN, n+1 NN (where n=[1:N]) from distance matrix... which should be expected
print('Neighbours found are in the same basis position in other cells, so by definition'
      ' are always the nth neighbour of central atom, where n = 2,4,6,...,even integer \n')


# ------------------------------------------------------------------------------------
# Create a neighbour list with specified neighbours in it, i.e. even neighbours only
# This can be used as an alternative to equivalent_atoms when constructing H,S
# Note: I don't think the 2nd point made sense conceptually but it's useful to keep
# the workflow
# ------------------------------------------------------------------------------------

#For each atom, create a list of even_neighbours
print('Atom i,  2,4,6 and 8th nearest neighbours of atom i in system:')
even_neighbours =  [2,4,6,8]  #list(set(computed_neighbours))
specific_nlist = structure.find_specific_neighbours(even_neighbours,r,al)
for atom in system:
    print (atom, specific_nlist[atom])
print('')

#Tabulated neighbour distances
expected_ndist = []
for key in constants.neighbour_distance(al):
    if key in even_neighbours: expected_ndist.append(constants.neighbour_distance(al)[key])

#Neighbour distances computed from specific neighbour list
computed_ndist = []
for atom in system:
    for natom in specific_nlist[atom]:
        separation = np.linalg.norm(r[natom, :] - r[atom, :])
        computed_ndist.append(separation)

#Round precison to 9 d.p.
computed_ndist = [ '%.9f' % elem for elem in computed_ndist ]
expected_ndist = [ '%.9f' % elem for elem in expected_ndist ]

#Confirm all atoms in specific neighbour list are at the distances expected
erroneous_values = set(computed_ndist) - set(expected_ndist)
empty = set()
if erroneous_values != empty :
    print('Erroneous neighbours in computed_ndist: ',erroneous_values,'\n')

#Add indices of atoms in central cell to specific neighbour list
for atom in system:
    specific_nlist[atom].append(atom)


# ---------------------------------------------------------
#Read in and solve H,S
# ---------------------------------------------------------

# Read in H and S generated by ENTOS
hfile='prim_555/h0.txt'
sfile='prim_555/overlap.txt'
h_data,s_data = hs.read_in_entos_hs(hfile,sfile)


nn_gamma_calculation = False
if nn_gamma_calculation:
    # Nearest neighbours for 2-atom system, excluding self-interactions
    system_nlist = {}
    for atom in system:
        system_nlist[atom] = [x for x in nlist[atom] if x != atom]

    print('Gamma-point NN H,S')
    kvec = [0., 0., 0.]
    h0 = hs.construct_NN_periodic_HS_kpoints(system, species, system_nlist, h_data, kvec, r)
    s0 = hs.construct_NN_periodic_HS_kpoints(system, species, system_nlist, s_data, kvec, r)
    hs.print_real_HS(h0,s0)
    eigen = linalg.eigh(h0, b=s0, lower=True, eigvals_only=True)
    print('Eigenvalues in eV:', eigen*constants.ha_2_ev)



gamma_calculation = True
if gamma_calculation:
    print('Gamma-point calculation using periodic h-builder')
    kvec = [0., 0., 0.]
    h0 = hs.construct_periodic_HS_kpoints(system, species, equivalent_atoms, h_data, kvec, r)
    s0 = hs.construct_periodic_HS_kpoints(system, species, equivalent_atoms, s_data, kvec, r)
    # h0 = hs.construct_periodic_HS_kpoints(system, species, specific_nlist, h_data, kvec, r)
    # s0 = hs.construct_periodic_HS_kpoints(system, species, specific_nlist, s_data, kvec, r)
    # hs.print_real_HS(h0,s0)

    if not np.allclose(h0, np.conjugate(h0.T), atol=1.e-6):
        print('h0 not hermitian, hence enforcing')
        h0 = 0.5 * (h0 + np.conjugate(h0.T))
    if not np.allclose(s0, np.conjugate(s0.T), atol=1.e-6):
        print('s not symmetric hence enforcing')
        s0 = 0.5 * (s0 + np.conjugate(s0.T))

    # Solve generalised eigenvalue problem and get band gap of system (at k=0)
    eigen = linalg.eigh(h0, b=s0, lower=True, eigvals_only=True)
    print('Eigenvalues in eV:', eigen*constants.ha_2_ev)
    bs.compute_bandgap(eigen)
    E_vbt = eigen[3]



bs_calculation = True
if bs_calculation:
    print('Band structure Calculations')
    bz_path = ['L-T','T-X','X-U','K-T']
    ksampling = [20,20,20,20]
    kpoints = bs.generate_band_structure_kpoints(bz_path,ksampling,material)

    eigen = []
    for kpoint in kpoints:
        h0 = hs.construct_periodic_HS_kpoints(system, species, equivalent_atoms, h_data, kpoint, r)
        s0 = hs.construct_periodic_HS_kpoints(system, species, equivalent_atoms, s_data, kpoint, r)
        # h0 = hs.construct_periodic_HS_kpoints(system, species, specific_nlist, h_data, kpoint, r)
        # s0 = hs.construct_periodic_HS_kpoints(system, species, specific_nlist, s_data, kpoint, r)
        h0 = 0.5 * (h0 + np.conjugate(h0.T))
        s0 = 0.5 * (s0 + np.conjugate(s0.T))
        w = linalg.eigh(h0, b=s0, lower=True, eigvals_only=True)
        eigen.append(w)

    Eg_indirect, k_cbb, k_vbt = bs.compute_indirect_bandgap(eigen,bz_path,ksampling)
    print('Indirect band gap for '+material+' (eV):',Eg_indirect*constants.ha_2_ev)
    print(' k_cbb, k_vbt (2pi/a):', k_cbb, k_vbt)

    # Apply a shift to the eigenvalues, such that the VBT is at 0 eV
    for ik in range(0,len(eigen)):
        eigen[ik] = eigen[ik]* constants.ha_2_ev + 8.641994

    # Output eigenvalues
    # total number of orbitals = basis_functions_per_atom * number of atoms in system
    # This assumes all atoms are same species
    norb = len(constants.basis_per_atom[material]) * len(system)
    bs.export_bandstructure_data(bz_path,ksampling,material,eigen)


