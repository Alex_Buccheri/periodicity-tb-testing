
import numpy as np
import constants
import zincblende as zb
import structure
import plot

# -----------------------------------------------------
# Test the function: Standardise_atomic_positions
#
# Create supercell
# Pick one atom as the origin of that cell
# Fold in atoms in other cells into the central cell
# -----------------------------------------------------

material = 'si'
atom_types = {'cation':'si','anion':'si'}

lattice_choice = 'orth'
nx = np.arange(0,3)
ny = np.arange(0,3)
nz = np.arange(0,3)

al = constants.al(material)
basis_positions,basis_species,lattice = zb.select_basis(atom_types,al,lattice_choice)
nbasis = len(basis_species)
basis={'pos':basis_positions,'species':basis_species}
natoms = constants.number_of_atoms(nbasis,nx,ny,nz)
r, cell_to_atom, atom_to_cell, species = structure.supercell(nx,ny,nz,basis,lattice)

label=lattice_choice+str(len(nx))+str(len(ny))+str(len(nz))
plot.output_xyz('testing/supercell_'+label+'.xyz',np.arange(0,natoms),r*constants.bohr_2_ang,species)


#Arb atom for origin of central cell
ia=104
r0 = r[ia,:]
plot.output_xyz('testing/origin.xyz',[ia],r*constants.bohr_2_ang,species)

#Output atoms in central cell
central_cell, surrounding_cells = structure.atoms_in_central_cell(r,r0,basis)
print(len(central_cell), len(surrounding_cells))
print( (np.asarray(central_cell)-r0)/al )


output_atoms = []
for atom in central_cell:
    output_atoms.append([x*constants.bohr_2_ang for x in atom])
plot.output_list_in_xyz('testing/inside_'+label+'.xyz',output_atoms)

#Output atoms surrounding central cell
output_atoms = []
for atom in surrounding_cells:
    output_atoms.append([x*constants.bohr_2_ang for x in atom])
plot.output_list_in_xyz('testing/outside_'+label+'.xyz',output_atoms)

#Translate atoms in cells outside the central cell, to the central cell
#Does not work correctly
#translated_system = structure.standardise_positions(r,r0,lattice)

translated_system = structure.alt_standardise_positions(r,r0,basis,lattice)
print(len(translated_system),len(surrounding_cells))

#Convert to ang for visu# alisation
output_atoms = []
for atom in translated_system:
    output_atoms.append([x*constants.bohr_2_ang for x in atom])
plot.output_list_in_xyz('testing/translated_'+label+'.xyz',output_atoms)
