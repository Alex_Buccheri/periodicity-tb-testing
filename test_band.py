#!/usr/bin/env python3

import bandstructure as bs


#Displacement vector path through the BZ of zincblende crystals
bz_path = ['L-T','T-X']
ksampling =[10,10]

kdis = bs.displacement_vectors(bz_path)

# print('BZ path, displacement vector between high-sym points')
# for ik in range(0,len(kdis)):
#     print(bz_path[ik],kdis[ik])
# print('')

material='si'
kpoint = bs.generate_band_structure_kpoints(bz_path,ksampling,material)
# print('Set of discrete k-points between:',bz_path)
# for k in kpoint:
#     print(k)
# print('')

#Two methods for printing lists
nkpoint = len(kpoint)
str_kpoint = [str(i) for i in kpoint]
for ik in range(0,nkpoint):
    print(str_kpoint[ik][1:-1])


for ik in range(0, nkpoint):
    print(ik, *kpoint[ik],sep=" ")




