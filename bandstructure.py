#!/usr/bin/env python3

#Modules
import numpy as np
import sys
from scipy import linalg

#My modules
import zincblende
import constants

# -----------------------------
# Generate band structure
# -----------------------------

def displacement_vectors(bz_path):
    dvector = []
    for path in bz_path:
        start,stop = path.split('-')
        k1 = zincblende.BZ.symmetry_point[start]
        k2 = zincblende.BZ.symmetry_point[stop]
        dvector.append(k2-k1)
    return dvector


# For a path through the Brillouin zone, compute discrete k-points
# in units of 2pi/a
# Pass bz_path = ['L-T','T-X'] and ksampling = [10,10], for example
def generate_band_structure_kpoints(bz_path,ksampling,material):
    kpoint = []
    unit = (2.*np.pi/constants.al(material))
    for ivec in range(0,len(bz_path)):
        start, stop = bz_path[ivec].split('-')
        k1 = zincblende.BZ.symmetry_point[start]*unit
        k2 = zincblende.BZ.symmetry_point[stop]*unit
        dvec = k2-k1
        nk = ksampling[ivec]
        dk = dvec/nk
        for ik in range(0,nk):
            kpoint.append(k1 + ik*dk)
    last_point = ''.join(bz_path)[-1:]
    kpoint.append(zincblende.BZ.symmetry_point[last_point]*unit)
    return kpoint


#Export band structure in eV
def export_bandstructure_data(bz_path,ksampling,material,eigen):
    kpoint = 0.
    unit = (2. * np.pi / constants.al(material))

    fid = open(file=material+'_bs.dat', mode='w')
    for ivec in range(0,len(bz_path)):
        start, stop = bz_path[ivec].split('-')
        k1 = zincblende.BZ.symmetry_point[start]*unit
        k2 = zincblende.BZ.symmetry_point[stop]*unit
        knorm  = np.linalg.norm(k2-k1)
        Nk = ksampling[ivec]
        for ik in range(0, Nk):
            ik_global = ik + (ivec * Nk)
            kpoint += ik * (knorm / float(Nk - 1))
            print(ik_global, *eigen[ik_global] , sep=" ",file=fid)
    fid.close()
    print('Eigenvalues written to file')
    return


def compute_bandgap(eigen):
    eigen = eigen * constants.ha_2_ev
    Ehomo = eigen[3]   #np.amax(eigen[eigen < 0]) - Can't assume VBT is zeroed
    Elumo = eigen[4]   #np.amin(eigen[eigen > 0])
    Eg = Elumo - Ehomo
    Eg_ref = 3.4  # Taken from some googled calculation, with the image on my desktop
    print('HOMO, LUMO, Computed Bulk band gap (gamma), Ref bulk Eg:')
    print(Ehomo, Elumo, Eg, Eg_ref,'\n')
    return

#bands[0:4] = occupied
#bands[4:] = unoccupied
def compute_indirect_bandgap(eigen,bz_path,ksampling):
    vbt = -10.
    cbb = 20.

    for ivec in range(0, len(bz_path)):
        start, stop = bz_path[ivec].split('-')
        k1 = zincblende.BZ.symmetry_point[start]
        k2 = zincblende.BZ.symmetry_point[stop]
        dvec = k2 - k1
        Nk = ksampling[ivec]
        dk = dvec / Nk

        for ik in range(0, Nk):
            ik_global = ik + (ivec * Nk)
            kpoint = k1 + ik*dk
            w = np.asarray(eigen[ik_global])
            if np.amax(w[:4]) > vbt:
                vbt = np.amax(w[:4])
                k_vbt = kpoint
            if np.amin(w[4:]) < cbb:
                cbb = np.amin(w[4:])
                k_cbb = kpoint
        Eg_indirect = cbb - vbt
    return Eg_indirect, k_cbb, k_vbt









